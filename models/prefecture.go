package models

type Prefecture int

const (
	PrefectureTokyo   Prefecture = 13
	PrefectureOsaka   Prefecture = 27
	PrefectureOkinawa Prefecture = 47
	PrefectureUnknown Prefecture = 0
)

func (p Prefecture) String() string {
	switch p {
	case PrefectureTokyo:
		return "東京"
	case PrefectureOsaka:
		return "大阪"
	case PrefectureOkinawa:
		return "沖縄"
	default:
		return "不明"
	}
}
