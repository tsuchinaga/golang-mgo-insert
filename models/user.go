package models

import "gopkg.in/mgo.v2/bson"

type User struct {
	Id         bson.ObjectId `bson:"_id"`
	Type       UserType      `bson:"type"`
	Prefecture Prefecture    `bson:"prefecture"`
	Valid      UserValid     `bson:"valid"`
}
