package models

type UserValid bool

const (
	UserValidTrue  UserValid = true
	UserValidFalse UserValid = false
)

func (v UserValid) String() string {
	switch v {
	case UserValidTrue:
		return "valid"
	case UserValidFalse:
		return "invalid"
	default:
		return "unknown"
	}
}
