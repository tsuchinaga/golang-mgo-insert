package models

type UserType string

const (
	UserTypeAdministrator UserType = "administrator"
	UserTypeNormal        UserType = "normal"
	UserTypeSpecial       UserType = "special"
	UserTypeUnknown       UserType = "unknown"
)

func (t UserType) String() string {
	switch t {
	case UserTypeAdministrator:
		return "administrator"
	case UserTypeNormal:
		return "normal"
	case UserTypeSpecial:
		return "special"
	default:
		return "unknown"
	}
}
