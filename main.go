package main

import (
	"gitlab.com/tsuchinaga/golang-mgo-insert/models"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
)

func main() {
	log.SetFlags(log.Lshortfile | log.LstdFlags)

	session, err := mgo.DialWithInfo(&mgo.DialInfo{
		Addrs:    []string{"golang-mgo-db"},
		Database: "golang-mgo",
		Username: "golang-mgo",
		Password: "golang-mgo",
	})
	if err != nil {
		log.Fatalln(err)
	}
	db := session.DB("golang-mgo")

	// drop
	if names, err := db.CollectionNames(); err != nil {
		log.Fatalln(err)
	} else {
		for _, name := range names {
			if name == "users" {
				if err := db.C("users").DropCollection(); err != nil {
					log.Fatalln(err)
				}
			}
		}
	}

	// insert
	if err := db.C("users").Insert(&models.User{
		Id:         bson.NewObjectId(),
		Type:       models.UserTypeAdministrator,
		Prefecture: models.PrefectureOsaka,
		Valid:      models.UserValidTrue,
	}); err != nil {
		log.Fatalln(err)
	}

	// find
	users := make([]models.User, 0)
	if err := db.C("users").Find(bson.M{}).All(&users); err != nil {
		log.Fatalln(err)
	}

	log.Println(users)
	for _, user := range users {
		if user.Type == "hoge" {
			log.Println(user.Id, "こいつ、hogeやぞ!!")
		}

		if user.Prefecture == models.PrefectureOsaka {
			log.Println(user.Id, "道頓堀")
		}

		if user.Valid == models.UserValidTrue {
			log.Println(user.Id, "有効!!")
		}
	}
}
